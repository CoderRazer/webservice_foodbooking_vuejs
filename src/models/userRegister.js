export default class UserRegister {
    constructor(name, email, password, phone, address) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }
}