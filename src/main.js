import Vue from 'vue';
import App from './App.vue';
import { router } from './router';
import store from './store';
// import Axios from 'axios';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import VeeValidate from 'vee-validate';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faHome,
  faUser,
  faUserPlus,
  faSignInAlt,
  faSignOutAlt
} from '@fortawesome/free-solid-svg-icons';
import firebase from 'firebase';
import LottiePlayer from 'lottie-player-vue'
var firebaseConfig = {
  apiKey: "AIzaSyBu-iropPcmKnW2EI6KiiwjY352i7v1VCM",
  authDomain: "foodbooking-48d85.firebaseapp.com",
  databaseURL: "https://foodbooking-48d85.firebaseio.com",
  projectId: "foodbooking-48d85",
  storageBucket: "foodbooking-48d85.appspot.com",
  messagingSenderId: "920691068388",
  appId: "1:920691068388:web:934eeef1e1f9506c6acbf1"
};
firebase.initializeApp(firebaseConfig);

library.add(faHome, faUser, faUserPlus, faSignInAlt, faSignOutAlt);

Vue.config.productionTip = false;

Vue.config.ignoredElements = [/^ion-/]

// TODO: Auto - Authentication
// Vue.prototype.$http = Axios;
// const user = localStorage.getItem('user');
// if (user){
//   Vue.prototype.$http.defaults.headers.common['Authorization'] = user.accessToken;
// }
// Auto - Authentication
Vue.use(LottiePlayer);
Vue.use(VeeValidate);
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');