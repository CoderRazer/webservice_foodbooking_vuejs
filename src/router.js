import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';

import Food from './components/foods/sub/Food.vue';
import Order from './components/orders/sub/Order.vue';

import store from './store';

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/profile',
      //lazy-loaded
      component: () => import('./views/Profile.vue')
    },
    {
      path: '/admin',
      name: 'admin',
      // lazy-loaded
      component: () => import('./views/BoardAdmin.vue')
    },
    {
      path: '/loginQR',
      name: 'loginQR',
      // lazy-loaded
      component: () => import('./views/LoginQR.vue')
    },
    {
      path: '/upload',
      name: 'upload',
      // lazy-loaded
      component: () => import('./views/Upload.vue')
    },

    // ================== Management ================= //
    {
      path: '/categoryManagement',
      // lazy-loaded
      component: () => import('./components/categories/CategoryManagement.vue'),
      children:[
        {
          path : '/categoryManagement/list',
          component: () => import('./components/categories/sub/CategoryList.vue')
        },
        {
          path : '/categoryManagement/add',
          component: () => import('./components/categories/sub/CategoryAdd.vue')
        },
        {
          path : '/categoryManagement/search',
          component: () => import('./components/categories/sub/CategorySearch.vue')
        }
      ]
    },
    {
      path: '/foodManagement',
      // lazy-loaded
      component: () => import('./components/foods/FoodManagement.vue'),
      children:[
        {
          path : '/foodManagement/list',
          component: () => import('./components/foods/sub/FoodList.vue'),
          children:[
            {
              path:'/foodManagement/food_detail/:id',
              name: "food-details",
              component: Food,
              props: true,
            }
          ]
        },
        {
          path : '/foodManagement/add',
          component: () => import('./components/foods/sub/FoodAdd.vue'),
        },
        {
          path : '/foodManagement/search',
          component: () => import('./components/foods/sub/FoodSearch.vue'),
        }
      ]
    },
    {
      path: '/userManagement',
      component: () => import('./components/users/UserManagement.vue'),
      children:[
        {
          path : '/userManagement/list',
          component: () => import('./components/users/sub/UserList.vue')
        },
        {
          path : '/userManagement/add',
          component: () => import('./components/users/sub/UserAdd.vue'),
        }
      ]
    },
    {
      path: '/orderManagement',
      component: () => import('./components/orders/OrderManagement.vue'),
      children:[
        {
          path : '/orderManagement/list',
          component: () => import('./components/orders/sub/OrderList.vue'),
          children:[
            {
              path:'/orderManagement/order_detail/:id',
              name: "order-details",
              component: Order,
              props: true,
            }
          ]
        }

      ]
    }

  ]
});

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login') 
  } else {
    next() 
  }
})