import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://2ade04a20fa7.ngrok.io/api/access';

class UserServiceRestAPI {

  getPublicContent() {
    return axios.get(API_URL + '/all');// don't need header by no authority this content
  }

  getUserBoard() {
    return axios.get(API_URL + '/user', { headers: authHeader() });
  }

  getProjectManagerBoard() {
    return axios.get(API_URL + '/pm', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + '/admin', { headers: authHeader() });
  }

}
export default new UserServiceRestAPI();